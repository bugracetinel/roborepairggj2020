﻿using UnityEngine;

public class Robot : MonoBehaviour
{
    public int id;
    [SerializeField] private Camera mainCamera;
    [SerializeField] public RobotType robotType;

    public enum RobotType
    {
        Player,
        Fat,
        Tall,
        Short
    }

    public string infoText;

    private Ray lastRay;
    private CharacterController characterController;
    private Health health;
    private AnimationControl animationControl;

    public bool isPlayer = false;
    private RaycastHit[] lastRayHits;

    public float speed = 6.0F;
    public float jumpSpeed = 6.0F;
    public float gravity = 15.0F;
    public float rotationSpeed = 180;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 rotation = Vector3.zero;

    private bool isInHandAnimation = false; //Only for the tall one


    private void Awake()
    {
        infoText = infoText.Replace("\\n", "\n");
        characterController = GetComponent<CharacterController>();
        health = GetComponent<Health>();
        animationControl = GetComponent<AnimationControl>();

        mainCamera = FindObjectOfType<Camera>();
    }
    private void Start()
    {
        GetRobotProperties(robotType);
    }

    public int GetHealth()
    {
        return health.GetHealth();
    }

    // Update is called once per frame
    void Update()
    {
        if (health.isDead) return;

        if (isPlayer)
        {
            if (Input.GetMouseButtonDown(0))
            {
                InteractWithOthers();
            }

            MoveDirection();
        }
    }

    public bool IsAlive()
    {
        return !health.isDead;
    }

    private void MoveDirection()
    {
        if (isInHandAnimation)
        {
            if (Input.GetButtonDown("Jump"))
            {
                animationControl.BackLocomotion();
                isInHandAnimation = false;
            }

            return;
        }

        float x = Input.GetAxis("Vertical");
        float z = Input.GetAxis("Horizontal");

        if (characterController.isGrounded)
        {
            moveDirection = new Vector3(-x, 0, z);
            moveDirection *= speed;
            RotatePlayer(x, z);
            if (Input.GetButtonDown("Jump"))
                Jump();
        }
        moveDirection.y -= gravity * Time.deltaTime;
        characterController.Move(moveDirection * Time.deltaTime);
    }

    private void RotatePlayer(float x, float z)
    {
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            float angle = Mathf.Atan2(-x, z) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0, angle, 0);
            //print("Angle: " + angle);
        }
    }

    private void Jump()
    {
        moveDirection.y = jumpSpeed;
        switch (robotType)
        {
            case RobotType.Tall:
                animationControl.HandAnimation();
                isInHandAnimation = true;
                break;
            default:
                animationControl.JumpAnimation();
                break;
        }
    }

    public void StopWalking()
    {
        animationControl.StopRobotRunning();
    }

    private bool InteractWithOthers()
    {
        GetMouseRay();
        lastRayHits = Physics.RaycastAll(lastRay);
        for (int i = 0; i < lastRayHits.Length; i++)
        {
            if (lastRayHits[i].collider.CompareTag("Robot"))
            {
                Health targetHealth = lastRayHits[i].collider.GetComponent<Health>();
                health.GiveHealth(1, targetHealth);
                return true;
            }
        }
        return false;
    }

    public void SetActive(bool isActive)
    {
        isPlayer = isActive;
    }

    private void GetMouseRay()
    {
        lastRay = mainCamera.ScreenPointToRay(Input.mousePosition);
    }

    private void GetRobotProperties(RobotType type)
    {
        switch (type)
        {
            case RobotType.Player:
                speed = 5.6F;
                jumpSpeed = 6.0F;
                gravity = 16.0F;
                break;
            case RobotType.Fat:
                speed = 4.0F;
                jumpSpeed = 3.0F;
                gravity = 20.0F;
                gravity = 15.0F;
                break;
            case RobotType.Tall:
                speed = 5F;
                jumpSpeed = 0.0F;
                gravity = 15.0F;
                break;
            case RobotType.Short:
                speed = 5.8F;
                jumpSpeed = 6;
                gravity = 16;
                break;
        }
    }
}
