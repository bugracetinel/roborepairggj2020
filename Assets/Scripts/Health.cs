﻿using UnityEngine;

public class Health : MonoBehaviour
{  
    [SerializeField] private int requiredHealth;
    [SerializeField] private int currentHealth = 0;
    private Robot robot;
    private AnimationControl animationControl;
    private CharacterController controller;
    private Health targetHealth;
    private UI ui;
    private GameManager gm;

    public bool isDead;

    private void Awake()
    {
        robot = GetComponent<Robot>();
        animationControl = GetComponent<AnimationControl>();
        controller = GetComponent<CharacterController>();
        gm = FindObjectOfType<GameManager>();
        ui = FindObjectOfType<UI>();
    }

    private void Start()
    {
        if (robot.isPlayer)
        {
            currentHealth = 4;
            return;
        }
        Die();
    }

    public void TargetHealth(Health target)
    {
        targetHealth = target;
    }

    public void GiveHealth(int amount, Health target)
    {
        if (GameManager.currentRobotId != 0) return;

        if (!target.isDead)
        {
            print("Trying to give health to alive robot");
            return;
        }

        currentHealth -= amount;
        target.GetHealth(amount);   
        if (currentHealth <= 0)
        {
            Die();
        }

        ui.UpdateUi();
    }

    public void GetHealth(int amount)
    {
        currentHealth += amount;
        if (currentHealth >= requiredHealth)
        {
            Resurrect();
            isDead = false;
        }

        ui.UpdateUi();
    }

    public int GetHealth()
    {
        return currentHealth;
    }

    private void Resurrect()
    {
        animationControl.ResurrectAnimation();
        AudioManager.instance.PlayAudio(2);
        controller.enabled = true;
    }

    private void Die()
    {
        if(robot.id == 0)
        {
            AudioManager.instance.PlayAudio(1);
        }

        animationControl.DeathAnimation();
        isDead = true;
        controller.enabled = false;

        if(robot.id == 0)
        {
            gm.ReportGameOver();
        }
    }
}
