﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public Text[] numberTexts;
    public Color aliveRobotColor, deadRobotColor;
    public Color[] baseColor;
    public GameObject[] hearts;

    public int selectedNumberFontSize = 124;
    public int unSelecedNumberFontSize = 80;
    public Text warningText;
    public Text infoText;

    bool isFadingOutText = false;
    int currentRobotId;
    Color warningTextStartColor;

    GameManager gm;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        warningTextStartColor = warningText.color;
        warningText.enabled = false;
    }


    private void Start()
    {
        UpdateUi();
    }

    // Update is called once per frame
    void Update()
    {
        float currentBrightness = numberTexts[currentRobotId].color.r;

        currentBrightness = 0.9f + Mathf.Sin(Time.timeSinceLevelLoad * 4f) * 0.1f;
        numberTexts[currentRobotId].color = new Color(currentBrightness * baseColor[currentRobotId].r,
                                                      currentBrightness * baseColor[currentRobotId].g,
                                                      currentBrightness * baseColor[currentRobotId].b);

        if(isFadingOutText)
        {
            float currentTextAlpha = warningText.color.a;
            currentTextAlpha -= Time.deltaTime * 2f;
            Color newColor = warningTextStartColor;
            newColor.a = currentTextAlpha;
            warningText.color = newColor;

            if(currentTextAlpha <= 0f)
            {
                isFadingOutText = false;
            }
        }

    }

    void FadeOutText()
    {
        isFadingOutText = true;
    }


    public void SetWarningText(string text, float duration)
    {
        CancelInvoke();
        warningText.text = text;
        warningText.color = warningTextStartColor;
        warningText.enabled = true;
        Invoke("FadeOutText", duration);
    }

    public void SetRobotNumberAliveOrDead(int id, bool isAlive)
    {
        numberTexts[id].color = isAlive ? aliveRobotColor : deadRobotColor;
        baseColor[id] = numberTexts[id].color;
    }

    public void SelectRobot(int currentRobotId)
    {
        this.currentRobotId = currentRobotId;
        int i;

        for (i = 0; i < numberTexts.Length; i++)
        {
            numberTexts[i].fontSize = (i == currentRobotId) ? selectedNumberFontSize : unSelecedNumberFontSize;
        }

        UpdateHealth();
        infoText.text = gm.GetCurrentRobot().infoText;
        //print(GameManager.currentRobotId);
        //infoText.enabled = (GameManager.currentRobotId == 0);
    }

    public void UpdateHealth()
    {
        if(!gm)
        {
            gm = FindObjectOfType<GameManager>();
        }

        Robot currentRobot = gm.GetCurrentRobot();
        int health = currentRobot.GetHealth();
        for (int i = 0; i < hearts.Length; i++)
        {
            if (i + 1 <= health)
            {
                hearts[i].SetActive(true);
            }
            else
            {
                hearts[i].SetActive(false);
            }
        }
    }

    public void UpdateUi()
    {
        int i;

        for (i = 0; i < numberTexts.Length; i++)
        {
            SetRobotNumberAliveOrDead(i, gm.robots[i].IsAlive());
        }

        UpdateHealth();
        
    }
}
