﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Intro : MonoBehaviour
{
    int state = 0;
    float lastTimeButtonPressed = 0; //Uses Time.realTimeSinceStartup

    public GameObject robotsCamera, gateCamera;
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        robotsCamera.SetActive(true);
        gateCamera.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.realtimeSinceStartup - lastTimeButtonPressed > 1 && Input.anyKeyDown)
        {
            state++;
            lastTimeButtonPressed = Time.realtimeSinceStartup;

            if (state == 1)
            {
                robotsCamera.SetActive(false);
                gateCamera.SetActive(true);
                text.text = "If they can pass this door, they will be free.";
            }
            else
            {
                SceneManager.LoadScene("Game");
            }
        }
    }
}
