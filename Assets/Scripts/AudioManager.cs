﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] AudioClip[] effects;
    AudioSource audioSource;

    public static AudioManager instance;

    private void Awake()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            PlayAudio(1);
        }
    }

    public void PlayAudio(int index)
    {
        audioSource.clip = effects[index];
        audioSource.Play();
    }
}
