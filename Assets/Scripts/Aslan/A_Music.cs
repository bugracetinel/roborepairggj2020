﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aslan
{
    [RequireComponent(typeof(AudioSource))]

	public class A_Music : MonoBehaviour 
	{
		static A_Music instance;

        new AudioSource audio;

		// Use this for initialization
		void Awake()
		{
            audio = GetComponent<AudioSource>();

            if (instance)
            {
                AudioClip audioClip = audio.clip;
                if (instance.audio.clip != audioClip && audioClip)
                {
                    instance.audio.clip = audioClip;
                    instance.audio.Play();
                }

                DestroyImmediate(gameObject);
                return;
            }
            
            //audio.mute = !A_PlayerPrefsBool.GetBool(playerPrefsIsOn, true);
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

	}
}
