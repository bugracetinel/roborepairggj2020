﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    public float speed = 5;
    float targetX;

    private Camera thisCamera;
    private Ray lastRay;

    // Start is called before the first frame update
    void Awake()
    {
        thisCamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        //return; //We won't use targeting yet
        //float currentX = transform.position.x;
        //if(Mathf.Abs(currentX - targetX) > 0.02f)
        //{
        //    currentX = Mathf.MoveTowards(currentX, targetX, Time.deltaTime * speed);
        //    SetZ(currentX);
        //}
    }

    public void SetTargetX(float targetX)
    {
        this.targetX = targetX;
    }

    public void SetZ(float z)
    {
        Vector3 currentPos = transform.position;
        currentPos.z = z;
        transform.position = currentPos;
    }
}
