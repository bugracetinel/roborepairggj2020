﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] GameObject doorParent;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Robot"))
        {
            StartCoroutine(OpenGates());
        }        
    }

    private IEnumerator OpenGates()
    {
        GameManager gm = FindObjectOfType<GameManager>();

        while (doorParent.transform.position.y > -10)
        {
            doorParent.transform.position -= new Vector3(0, 4, 0) * Time.deltaTime;
            yield return null;
        }

        gm.ReportPassLevel();
    }
}
