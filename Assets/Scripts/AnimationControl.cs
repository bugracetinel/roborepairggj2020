﻿using UnityEngine;

public class AnimationControl : MonoBehaviour
{
    private Robot robot;
    private Health health;
    private Animator animator;
    private float localSpeedZ;
    private CharacterController characterCont;
    private Vector3 localVelocity;

    private int[] animIDs = new int[7];

    private void Awake()
    {
        characterCont = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        health = GetComponent<Health>();
        robot = GetComponent<Robot>();

        GetAnimIDs();
    }

    private void Update()
    {
        if (!health.isDead && robot.isPlayer)
        {
            MatchAnimation();
        }
    }

    private void MatchAnimation()
    {
        localVelocity = transform.InverseTransformDirection(characterCont.velocity);
        localSpeedZ = Mathf.Abs(localVelocity.z);
        animator.SetFloat(animIDs[0], localSpeedZ);
    }

    public void StopRobotRunning()
    {
        animator.SetFloat(animIDs[0], 0);
    }

    public void DeathAnimation()
    {
        animator.SetTrigger(animIDs[1]);
    }

    public void ResurrectAnimation()
    {
        animator.SetTrigger(animIDs[4]);
    }

    public void JumpAnimation()
    {
        animator.SetTrigger(animIDs[5]);
        animator.ResetTrigger(animIDs[3]);
    }

    public void HandAnimation()
    {
        animator.applyRootMotion = true;
        animator.ResetTrigger(animIDs[3]);
        animator.SetTrigger(animIDs[6]);
    }

    public void BackLocomotion()
    {
        animator.applyRootMotion = false;
        animator.ResetTrigger(animIDs[6]);
        animator.SetTrigger(animIDs[3]);
    }

    private void GetAnimIDs()
    {
        animIDs[0] = Animator.StringToHash("forwardSpeed");
        animIDs[1] = Animator.StringToHash("die");
        animIDs[2] = Animator.StringToHash("giveEnergy");
        animIDs[3] = Animator.StringToHash("backLocomotion");
        animIDs[4] = Animator.StringToHash("resurrect");
        animIDs[5] = Animator.StringToHash("jump");
        animIDs[6] = Animator.StringToHash("handsUp");
    }
}
