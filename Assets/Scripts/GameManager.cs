﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] public Robot[] robots;
    public static int currentRobotId;

    GameCamera gameCamera;
    UI ui;
    bool isGameOver = false;
    float gameOverTime; //Uses Time.realtimeSinceStartup. The time when the game was over. We use this for checking to pressing any key to restart
    private void Awake()
    {
        currentRobotId = 0;
        gameCamera = FindObjectOfType<GameCamera>();
        ui = FindObjectOfType<UI>();       
    }

    // Start is called before the first frame update
    void Start()
    {
        ui.SelectRobot(currentRobotId);
        ChangeRobot(currentRobotId);
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_STANDALONE

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

#endif

        if(isGameOver && Time.realtimeSinceStartup - gameOverTime > 3 && Input.anyKeyDown)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            ChangeRobot(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ChangeRobot(1);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            ChangeRobot(2);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            ChangeRobot(3);
        }
        gameCamera.SetZ(GetCurrentRobot().transform.position.z);


        //gameCamera.SetTargetX(robots[currentRobotId].transform.position.x);
    }

    void ChangeRobot(int newRobotId)
    {
        if(!robots[newRobotId].IsAlive())
        {
            //print("Robot " + (newRobotId + 1).ToString()  + " is not alive yet...");
            ui.SetWarningText("Robot " + (newRobotId + 1).ToString()  + " is not alive yet...", 1.5f);
            return;
        }

        if(currentRobotId != newRobotId)
        {
            GetCurrentRobot().StopWalking();
        }

        currentRobotId = newRobotId;

        int i;

        for(i = 0; i < robots.Length; i++)
        {
            robots[i].SetActive(i == currentRobotId);
        }

        gameCamera.SetZ(GetCurrentRobot().transform.position.z);
        ui.SelectRobot(currentRobotId);
        //gameCamera.transform.SetParent(GetCurrentRobot().transform);
    }

    public Robot GetCurrentRobot()
    {
        return robots[currentRobotId];
    }

    public void ReportGameOver()
    {
        ui.SetWarningText("Game Over", 300.0f);
        gameOverTime = Time.realtimeSinceStartup;
        isGameOver = true;
    }

    public void ReportPassLevel()
    {
        ui.SetWarningText("LEVEL PASSED!", 3);
        Invoke("ChangeSceneToEnd", 3);
    }

    void ChangeSceneToEnd()
    {
        SceneManager.LoadScene("Credits");
    }
}
