﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Health playerHealth = other.GetComponent<Health>();
            playerHealth.GetHealth(1);
            AudioManager.instance.PlayAudio(0);
            Destroy(gameObject);
        }
    }
}
