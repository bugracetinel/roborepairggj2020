﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    int state = 0;
    public GameObject sistersAreFreeText, creditsText;

    float lastTimeButtonPressed = 0; //Uses Time.realTimeSinceStartup

    private void Awake()
    {
        sistersAreFreeText.SetActive(true);
        creditsText.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        lastTimeButtonPressed = Time.realtimeSinceStartup;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.realtimeSinceStartup - lastTimeButtonPressed > 2 && Input.anyKeyDown)
        {
            state++;
            lastTimeButtonPressed = Time.realtimeSinceStartup;

            if(state == 1)
            {
                sistersAreFreeText.SetActive(false);
                creditsText.SetActive(true);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
